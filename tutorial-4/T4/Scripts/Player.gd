extends KinematicBody2D

export (int) var speed = 400
export (int) var projectile_speed = 800
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400
export (bool) var die = false

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")
onready var projectile = load("res://Scenes/Projectile.tscn")
onready var control = $Control



func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		
	if Input.is_action_just_pressed("fire"):
		shoot()

func shoot():
	var projectile_vel = control.get_local_mouse_position().normalized() * projectile_speed
	var fire_ball = projectile.instance()
	fire_ball.velocity = projectile_vel
	add_child_below_node(get_tree().get_root().get_node("Level 1"), fire_ball)

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")
		
func _on_Area2D_body_entered(body):
	print(body.get_name())
	if "Bee" in body.get_name():
		suicide()

func suicide():
	get_tree().change_scene(str("res://Scenes/Level 1.tscn"))

func win():
	get_tree().change_scene(str("res://Scenes/Win Screen.tscn"))
