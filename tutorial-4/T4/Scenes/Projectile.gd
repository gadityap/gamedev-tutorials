extends KinematicBody2D


#export (Vector2) var direction = Vector2.ZERO
export (Vector2) var velocity = Vector2.ZERO

func _physics_process(delta):
	move_and_slide(velocity, Vector2(0,-1))

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Area2D_body_entered(body):
	if body.get_name() != "Player":
		queue_free()
	if "Bee" in body.get_name():
		body.queue_free()
