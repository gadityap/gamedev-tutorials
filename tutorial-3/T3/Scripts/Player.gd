extends KinematicBody2D

export (int) var speed = 400
export (int) var dash_speed = 1000
export (int) var acceleration = 5
export (int) var deacceleration = 9
export (int) var jump_speed = -600
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()

#Inputs
var direction = Vector2()
var jumped = false
var double_jumped = false

const dash_time_ms = 500
const dash_cooldown_s = 1
var dash_cooldown = 0
var dash_direction = 0
var last_left = 0.0
var last_right = 0.0

func update_input(delta):
	direction = Vector2()
	if Input.is_action_just_pressed('up'):
		jumped = true
	else:
		jumped = false
	if Input.is_action_pressed('right'):
		direction.x += 1
	if Input.is_action_pressed('left'):
		direction.x -= 1
		
	#Calculate dash
	var current_time = OS.get_system_time_msecs()
	if Input.is_action_just_pressed('left') and dash_cooldown <= 0:
		if current_time - last_left < dash_time_ms:
			dash_direction = -1
			dash_cooldown = dash_cooldown_s
		last_left = current_time
	elif Input.is_action_just_pressed('right') and dash_cooldown <= 0:
		if current_time - last_right < dash_time_ms:
			dash_direction = 1
			dash_cooldown = dash_cooldown_s
		last_right = current_time
	else:
		dash_direction = 0
	dash_cooldown -= delta
	dash_cooldown = 0.0 if dash_cooldown <= 0.0 else dash_cooldown

func _physics_process(delta):
	update_input(delta)
	
	#If not moving, deaccelerate
	var acc = acceleration if direction.x != 0 else deacceleration
	
	#Calculate X velocity
	var targetX = Vector2(direction.x * speed, 0)
	var horizontal_velocity = Vector2(velocity.x, 0)
	horizontal_velocity.x += dash_speed * dash_direction
	dash_cooldown -= delta
	horizontal_velocity = horizontal_velocity.linear_interpolate(targetX, acc * delta)
	
	#Calculate Y velocity
	var vertical_velocity = velocity.y + delta * GRAVITY
	if jumped:
		if is_on_floor():
			vertical_velocity = jump_speed
		elif not double_jumped:
			vertical_velocity = jump_speed
			double_jumped = true
	if is_on_floor():
		double_jumped = false
	
	#Move
	velocity = move_and_slide(Vector2(horizontal_velocity.x, vertical_velocity), UP)
